import javax.swing.JOptionPane;

public class RunnerHomeWork {

	public static void main(String[] args) {
		String in;
		in = JOptionPane.showInputDialog(null, "What is your name: ");
		while (in.equals("")) {
			in = JOptionPane.showInputDialog(null, "What is your name: ");
		}
		JOptionPane.showMessageDialog(null, "Hello " + in);
	}

}
